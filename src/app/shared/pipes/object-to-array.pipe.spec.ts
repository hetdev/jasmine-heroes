import {ObjectToArrayPipe} from './object-to-array.pipe';


describe('ObjectToArrayPipe Test', () => {
  const arrayObjects = [
    {id: 1},
    {id: 2},
    {id: 3}
  ];
  const object = {id: 1, _id: 2, idd: 3};
  const objectDiff = {id: 1, _id: 2, idd: 3, iddd: 3};

  it('create an instance', () => {
    const pipe = new ObjectToArrayPipe();
    expect(pipe).toBeTruthy();
  });

  it('Pipe array length', () => {
    const pipe = new ObjectToArrayPipe();
    const pipeResult = pipe.transform(object);
    expect(pipeResult.length).toEqual(arrayObjects.length);
  });

  it('Pipe array length - not the same', () => {
    const pipe = new ObjectToArrayPipe();
    const pipeResult = pipe.transform(objectDiff);
    expect(pipeResult.length).not.toEqual(arrayObjects.length);
  });

  it('Pipe array - first object', () => {
    const pipe = new ObjectToArrayPipe();
    const pipeResult = pipe.transform(object);
    expect(pipeResult[0]).toEqual(arrayObjects[0].id);
  });

  it('Pipe array - last object', () => {
    const pipe = new ObjectToArrayPipe();
    const pipeResult = pipe.transform(object);
    expect(pipeResult[pipeResult.length - 1]).toEqual(arrayObjects[arrayObjects.length - 1].id);
  });


});
