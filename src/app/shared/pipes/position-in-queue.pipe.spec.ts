import { PositionInQueuePipe } from './position-in-queue.pipe';

describe('PositionInQueuePipe Test', () => {

  it('create an instance', () => {
    const pipe = new PositionInQueuePipe();
    expect(pipe).toBeTruthy();
  });

  it('Testing PositionInQueuePipe 1st', () => {
    const pipe = new PositionInQueuePipe();
    const case1 = pipe.transform(1);
    expect(case1).toEqual('1st');
  });
  it('Testing PositionInQueuePipe 2nd', () => {
    const pipe = new PositionInQueuePipe();
    const case2 = pipe.transform(2);
    expect(case2).toEqual('2nd');
  });
  it('Testing PositionInQueuePipe 3rd', () => {
    const pipe = new PositionInQueuePipe();
    const case3 = pipe.transform(3);
    expect(case3).toEqual('3rd');
  });
  it('Testing PositionInQueuePipe 4th', () => {
    const pipe = new PositionInQueuePipe();
    const case4 = pipe.transform(4);
    expect(case4).toEqual('4th');
  });

  it('Testing PositionInQueuePipe 10th', () => {
    const pipe = new PositionInQueuePipe();
    const case4 = pipe.transform(10);
    expect(case4).toEqual('10th');
  });
});
