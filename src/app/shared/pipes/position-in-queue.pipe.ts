import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'positionInQueue'
})
export class PositionInQueuePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 1) {
      return '1st';
    } else if (value === 2) {
      return '2nd';
    } else if (value === 3) {
      return '3rd';
    }
    return value + 'th';
  }

}
