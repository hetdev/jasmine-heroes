import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'objectToArray'
})
export class ObjectToArrayPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    const arrayResult = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        const element = value[key];
        arrayResult.push(element);
      }
    }
    return arrayResult;
  }

}
