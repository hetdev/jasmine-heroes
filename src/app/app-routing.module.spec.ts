import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StoreModule} from '@ngrx/store';
import {RouterTestingModule} from '@angular/router/testing';
import {heroReducer} from './heroes/hero.reducer';
import {routes} from './app-routing.module';
import {AppComponent} from './app.component';
import {Router} from '@angular/router';
import {APP_BASE_HREF, Location} from '@angular/common';
import {AppModule} from './app.module';


describe('Routing Test', () => {
  let fixture: ComponentFixture<AppComponent>;
  let location: Location;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes),
        StoreModule.forRoot({heroes: heroReducer}),
        AppModule],
      declarations: [],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}]
    });

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    router.initialNavigation();

  });

  it('can navigate to home (async)', async(() => {
    router
      .navigate([''])
      .then(() => {
        expect(location.path().endsWith('/dashboard')).toBe(true);
      }).catch(e => console.log(e));
  }));

  it('can navigate to dashboard from /', async(() => {
    router
      .navigate([''])
      .then(() => {
        expect(location.path().endsWith('/dashboard')).toBe(true);
      }).catch(e => console.log(e));
  }));

  it('can navigate to dashboard from /dashboard', async(() => {
    router
      .navigate(['/dashboard'])
      .then(() => {
        expect(location.path().endsWith('/dashboard')).toBe(true);
      }).catch(e => console.log(e));
  }));


  it('can navigate to dashboard from /error', async(() => {
    router
      .navigate(['/error'])
      .then(() => {
        expect(location.path().endsWith('/dashboard')).toBe(true);
      }).catch(e => console.log(e));
  }));

  it('can navigate to hero-detail', async(() => {
    router
      .navigate(['/hero-detail/1'])
      .then(() => {
        expect(location.path().endsWith('/hero-detail/1')).toBe(true);
      }).catch(e => console.log(e));
  }));

  it('can navigate to dashboard from failed hero-detail', async(() => {
    router
      .navigate(['/hero-detailt/1'])
      .then(() => {
        expect(location.path().endsWith('/dashboard')).toBe(true);
      }).catch(e => console.log(e));
  }));

});
