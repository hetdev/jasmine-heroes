import * as heroActions from './hero.actions';
import {heroReducer} from './hero.reducer';
import {Hero} from '../shared/models/hero.model';

const mockDataInitialState = {};

const heroesMockData = {
  0:
    Object({
      '_name': 'Anthony Stark',
      '_height': 1.98,
      '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg',
      '_nickname': 'Iron Man',
      '_id': 1
    })
};

describe('HeroReducer Test', () => {

  it('should return the default state', () => {
    expect(heroReducer([], new heroActions.GetHeroesSuccess(mockDataInitialState))).toEqual(mockDataInitialState);
  });


  it('should return the new state with heroes', () => {
    expect(heroReducer([], new heroActions.GetHeroesSuccess(heroesMockData))).toEqual(heroesMockData);
  });

});

