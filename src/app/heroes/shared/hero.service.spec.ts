import {TestBed, inject} from '@angular/core/testing';

import {HeroService} from './hero.service';
import {HttpModule} from '@angular/http';


describe('HeroService Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule
      ],
      providers: [HeroService]
    });
  });


  it('getHeroes() Observable<Array<Hero>>', inject([HeroService], (service: HeroService) => {


    service.loadHeroes().subscribe((heroes) => {
      expect(heroes.length).toBe(9);
      expect(heroes[0]._name).toEqual('Anthony Stark');
      expect(heroes[0]._height).toBe(1.98);
    });
  }));

  it('HeroServiceCreated', inject([HeroService], (service: HeroService) => {
    expect(service).toBeTruthy();
  }));
});
