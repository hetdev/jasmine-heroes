import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Hero} from '../../shared/models/hero.model';
import 'rxjs/add/operator/map';

@Injectable()
export class HeroService {
  private _baseUrl = 'https://udem.herokuapp.com/heroes';

  readonly foot = 0.3048;
  readonly inch = 0.0254;

  constructor(private http: Http) {
  }

  loadHeroes(): Observable<Hero[]> {
    return this.http.get(this._baseUrl).map(data => this.refactorHeroes(data.json()));
  }

  refactorHeroes(value: any): any {

    const arrayResult = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        const element = value[key];
        element._id = Number(key) + 1;
        element._height = this.heightToMeters(element._height);
        arrayResult.push(element);
      }
    }
    return arrayResult;
  }

  heightToMeters(height: any): any {
    const strHeightSplitted = String(height).split('.');
    const meters = Number(strHeightSplitted[0]) * this.foot;
    const inchs = Number(strHeightSplitted[0]) * this.inch;
    const newHeight =  Math.round((meters + inchs) * 100) / 100;
    return newHeight;
  }


}
