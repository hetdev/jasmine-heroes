// import {EffectsTestingModule, EffectsRunner} from '@ngrx/effects/testing';
import {HeroEffects} from './hero.effects';

import {HeroService} from './shared/hero.service';

import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {hot, cold} from 'jasmine-marbles';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import * as heroActions from './hero.actions';

import {EffectsMetadata, getEffectsMetadata} from '@ngrx/effects';
import {HttpModule} from '@angular/http';
import {ReplaySubject} from 'rxjs/ReplaySubject';


const heroesMockData = [
  {
    '_name': 'Anthony Stark',
    '_height': 1.98,
    '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg',
    '_nickname': 'Iron Man',
    '_id': 1
  }, {
    '_name': 'Peter Parker',
    '_height': 1.65,
    '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/538cd33e15ab7/standard_xlarge.jpg',
    '_nickname': 'Spider-Man',
    '_id': 2
  }, {
    '_name': 'James Howlett',
    '_height': 1.65,
    '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf/standard_xlarge.jpg',
    '_nickname': 'Wolverine',
    '_id': 3
  }, {
    '_name': 'Robert Bruce ',
    '_height': 1.65,
    '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0/standard_xlarge.jpg',
    '_nickname': 'Hulk',
    '_id': 4
  }, {
    '_name': 'Thor Odinson',
    '_height': 1.98,
    '_picture': 'http://x.annihil.us/u/prod/marvel/i/mg/5/a0/537bc7036ab02/standard_xlarge.jpg',
    '_nickname': 'Thor',
    '_id': 5
  }, {
    '_name': ' Piotr Rasputin',
    '_height': 1.98,
    '_picture': 'http://x.annihil.us/u/prod/marvel/i/mg/6/e0/51127cf4b996f/standard_xlarge.jpg',
    '_nickname': 'COLOSSUS',
    '_id': 6
  }, {
    '_name': 'Ororo Munroe',
    '_height': 1.65,
    '_picture': 'http://x.annihil.us/u/prod/marvel/i/mg/c/c0/537bc5db7c77d/standard_xlarge.jpg',
    '_nickname': 'Storm',
    '_id': 7
  }, {
    '_name': 'Remy Etienne ',
    '_height': 1.98,
    '_picture': 'http://i.annihil.us/u/prod/marvel/i/mg/9/40/537baad144c79/standard_xlarge.jpg',
    '_nickname': 'Gambit',
    '_id': 8
  }, {
    '_name': 'Norrin Radd',
    '_height': 1.98,
    '_picture': 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-128.png',
    '_nickname': 'Silver Surfer',
    '_id': 9
  }
];

describe('HeroEffects Test', () => {
  let effects: HeroEffects;
  let actionObservable: Observable<any>;
  let actions: Observable<any>;
  let actionsEffects: Observable<any>;
  let metadata: EffectsMetadata<HeroEffects>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        HeroEffects,
        HeroService,
        provideMockActions(() => actions),
      ],
    });

    effects = TestBed.get(HeroEffects);
    metadata = getEffectsMetadata(effects);
  });

  it('should return list of heroes', () => {
    actionsEffects = new ReplaySubject(4);

    actionsEffects.next(new heroActions.GetHeroes({}));

    effects.getHeroes.subscribe(result => {
      expect(result).toEqual(new heroActions.GetHeroesSuccess(heroesMockData));
    });
  });


  it('false is false', () => expect(false).toBe(!true));

  it('true is true', () => expect(true).toBe(true));


  // Just to check if is dispatching any action hetdev
  it('getHeroes dispatch any action(getHeroesSuccess)', () => {
    expect(metadata.getHeroes).toEqual({dispatch: true});
  });

  it('updateHero dispatch any action(updateHeroSuccess)', () => {
    expect(metadata.updateHero).toEqual({dispatch: true});
  });

});

