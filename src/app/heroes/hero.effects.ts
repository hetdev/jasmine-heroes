import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';


import * as heroActions from './hero.actions';
import {HeroService} from './shared/hero.service';
import {UpdateHeroSuccess} from './hero.actions';

export type Action = heroActions.All;

@Injectable()
export class HeroEffects {
  constructor(private actions: Actions, private heroService: HeroService) {

  }

  @Effect()
  getHeroes: Observable<Action> = this.actions.ofType(heroActions.GET_HEROES)
    .map((action: heroActions.GetHeroes) => action.payload)
    .mergeMap(payload => this.heroService.loadHeroes())
    .map(heroes => {
      return new heroActions.GetHeroesSuccess(heroes);
    });

  @Effect()
  updateHero: Observable<Action> = this.actions.ofType(heroActions.UPDATE_HERO)
    .map((action: heroActions.UpdateHero) => action.payload)
    .map(hero => new heroActions.UpdateHeroSuccess(hero)
    );

}
