import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MainComponent} from './main.component';
import {Store, StoreModule} from '@ngrx/store';
import {heroReducer} from '../hero.reducer';
import {AppState} from '../../shared/interfaces/app-state';
import {HeroListComponent} from '../hero-list/hero-list.component';
import {ObjectToArrayPipe} from '../../shared/pipes/object-to-array.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {PositionInQueuePipe} from '../../shared/pipes/position-in-queue.pipe';
import * as heroActions from '../hero.actions';

describe('MainComponent Test', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;
  let store: Store<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({heroes: heroReducer}),
        RouterTestingModule],
      declarations: [MainComponent,
        HeroListComponent,
        ObjectToArrayPipe,
        PositionInQueuePipe]
    })
      .compileComponents();

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should created', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch an action to load data when created', () => {
    const action = new heroActions.GetHeroes({});
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should dispatch an action(GetHeroes) to load data when created not (GetHeroesSuccess)', () => {
    const action = new heroActions.GetHeroesSuccess([]);

    expect(store.dispatch).not.toHaveBeenCalledWith(action);
  });

});
