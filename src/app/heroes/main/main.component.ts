import {Component, OnInit} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {Hero} from '../../shared/models/hero.model';
import * as heroActions from '../hero.actions';
import {AppState} from '../../shared/interfaces/app-state';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  heroesList: Observable<Hero>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.heroesList = this.store.select('heroes');
    this.heroesList.subscribe(res => {
      if (res === undefined) {
        this.getHeroes();
      }
    });
  }

   getHeroes() {
    this.store.dispatch(new heroActions.GetHeroes({}));
  }
}
