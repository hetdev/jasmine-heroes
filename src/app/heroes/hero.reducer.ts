import * as heroActions from './hero.actions';
import * as _ from 'lodash';

export type Action = heroActions.All;

export function heroReducer(state: any, action: Action) {
  // console.log(action.type, state);
  switch (action.type) {
    case heroActions.GET_HEROES: {
      return {...state};
    }

    case heroActions.GET_HEROES_SUCCESS: {
      return {...state, ...action.payload};
    }

    case heroActions.UPDATE_HERO_SUCCESS: {

      const arrayResult = [];
      _.forEach(state, function (value, key) {
        if (value._id === action.payload._id) {
          value = action.payload;
        }
        arrayResult.push(value);
      });

      return arrayResult;
      // return state.map(hero => {
      //   return hero.id === action.payload._id ? Object.assign({}, hero, action.payload) : hero;
      // });
    }

    default:
      return state;
  }
}
