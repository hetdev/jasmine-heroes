import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import * as heroActions from '../hero.actions';
import {AppState} from '../../shared/interfaces/app-state';
import {Store} from '@ngrx/store';
import {Hero} from '../../shared/models/hero.model';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  hero: Hero;

  heroesList: Observable<Hero>;
  heroListObject: any;


  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private location: Location,
              private toastr: ToastrService,
              private router: Router) {
  }

  ngOnInit() {
    this.heroesList = this.store.select('heroes');
    this.heroesList.subscribe(res => {
      if (res === undefined) {
        this.store.dispatch(new heroActions.GetHeroes([]));
      }
      if (res) {
        this.heroListObject = res;
        this.getHero();
      }
    });

  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.hero = this.getHeroesFromStore(id);
    // console.log(this.hero);
  }

  getHeroesFromStore(id: any): any {
    let hero;
    _.forEach(this.heroListObject, function (value, key) {
      if (Number(value._id) === id) {
        hero = value;
      }
    });

    return hero;
  }

  updateHero() {
    this.hero._height = Math.round((this.hero._height) * 100) / 100;
    this.store.dispatch(new heroActions.UpdateHero(this.hero
    ));
    this.toastr.info('Hero updated');
  }

  goBack(): void {
    this.location.back();
  }

}
