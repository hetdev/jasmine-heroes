import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HeroDetailComponent} from './hero-detail.component';
import {FormsModule} from '@angular/forms';
import {Store, StoreModule} from '@ngrx/store';
import {heroReducer} from '../hero.reducer';
import {RouterTestingModule} from '@angular/router/testing';
import {ToastrModule} from 'ngx-toastr';
import {Location} from '@angular/common';
import {AppState} from '../../shared/interfaces/app-state';

describe('HeroDetailComponent Test', () => {
  let component: HeroDetailComponent;
  let fixture: ComponentFixture<HeroDetailComponent>;
  let store: Store<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        StoreModule.forRoot({heroes: heroReducer})],
      declarations: [HeroDetailComponent],
      providers: [
        Location
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should created', () => {
    expect(component).toBeTruthy();
  });

});
