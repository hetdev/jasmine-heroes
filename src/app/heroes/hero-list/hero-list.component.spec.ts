import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HeroListComponent} from './hero-list.component';
import {ObjectToArrayPipe} from '../../shared/pipes/object-to-array.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {PositionInQueuePipe} from '../../shared/pipes/position-in-queue.pipe';

describe('HeroListComponent Test', () => {
  let component: HeroListComponent;
  let fixture: ComponentFixture<HeroListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [HeroListComponent,
        ObjectToArrayPipe,
        PositionInQueuePipe],
      providers: []
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
