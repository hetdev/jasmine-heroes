import {Action} from '@ngrx/store';
import {Hero} from '../shared/models/hero.model';

export const GET_HEROES = '[Heroes] get';
export const GET_HEROES_SUCCESS = '[Heroes] get success';
export const UPDATE_HERO = '[Hero] update';
export const UPDATE_HERO_SUCCESS = '[Hero] update success';

export class GetHeroes implements Action {
  readonly type = GET_HEROES;

  constructor(public payload: any) {
  }
}

export class GetHeroesSuccess implements Action {
  readonly type = GET_HEROES_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UpdateHero implements Action {
  readonly type = UPDATE_HERO;

  constructor(public payload: any) {
  }
}

export class UpdateHeroSuccess implements Action {
  readonly type = UPDATE_HERO_SUCCESS;

  constructor(public payload: any) {
  }
}

export type All = GetHeroes | GetHeroesSuccess | UpdateHero | UpdateHeroSuccess;
