import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HeroService} from './heroes/shared/hero.service';

import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {HeroEffects} from './heroes/hero.effects';
import {heroReducer} from './heroes/hero.reducer';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {ObjectToArrayPipe} from './shared/pipes/object-to-array.pipe';
import {MainComponent} from './heroes/main/main.component';

import {AppRoutingModule} from './app-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeroListComponent} from './heroes/hero-list/hero-list.component';
import {HeroDetailComponent} from './heroes/hero-detail/hero-detail.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PositionInQueuePipe} from './shared/pipes/position-in-queue.pipe';


@NgModule(<NgModule>{
  declarations: [
    AppComponent,
    ObjectToArrayPipe,
    MainComponent,
    HeroListComponent,
    HeroDetailComponent,
    PositionInQueuePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    EffectsModule.forRoot([HeroEffects]),
    StoreModule.forRoot({heroes: heroReducer}),
    StoreDevtoolsModule.instrument({maxAge: 20}),
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()

  ],
  providers: [HeroService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
